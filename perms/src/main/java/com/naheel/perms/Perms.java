package com.naheel.perms;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;

import java.util.ArrayList;

public class Perms extends Activity {

    private static PermsListener l;

    public static void ask(Context c, PermsListener l, String... perms) {
        Perms.l = l;
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            l.onNotSupported();
            return;
        }
        Intent i = new Intent(c, Perms.class);
        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        i.putExtra("perms", perms);
        c.startActivity(i);
    }

    public interface PermsListener {
        void onGranted();
        void onDenied();
        void onNotSupported();
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ArrayList<String> perms1 = new ArrayList<>();
        String[] perms = getIntent().getExtras().getStringArray("perms");
        assert perms != null;
        for (String s : perms)
            if (checkSelfPermission(s) != PackageManager.PERMISSION_GRANTED)
                perms1.add(s);
        if (perms1.size() == 0) { l.onGranted(); finish(); return; }
        requestPermissions(perms1.toArray(new String[perms1.size()]), 0);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case 0: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.
                    l.onGranted();
                    finish();
                } else {
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                    l.onDenied();
                    finish();
                }
                //return;
            }
            // other 'case' lines to check for other
            // permissions this app might request
        }
    }


}
