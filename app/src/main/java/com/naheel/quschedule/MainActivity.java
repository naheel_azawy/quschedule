package com.naheel.quschedule;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.provider.CalendarContract;
import android.text.method.LinkMovementMethod;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ScrollView;
import android.widget.TextView;

import com.naheel.perms.Perms;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;
import com.wdullaer.materialdatetimepicker.time.TimePickerDialog;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

public class MainActivity extends Activity {

    TextView tv;
    ScrollView sv;
    EditText et;
    MainActivity c = this;
    QuScheduleInterface q;
    List<Uri> calUris = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Perms.ask(this, new Perms.PermsListener() {
                    @Override
                    public void onGranted() {
                        work();
                    }

                    @Override
                    public void onDenied() {
                        c.finish();
                    }

                    @Override
                    public void onNotSupported() {
                        onGranted();
                    }
                },
                Manifest.permission.WRITE_EXTERNAL_STORAGE,
                Manifest.permission.READ_CALENDAR,
                Manifest.permission.WRITE_CALENDAR
        );

    }

    void work() {
        setContentView(R.layout.activity_main);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);

        tv = findViewById(R.id.tv);
        et = findViewById(R.id.et);
        sv = findViewById(R.id.sv);
        tv.setMovementMethod(LinkMovementMethod.getInstance());

        try {
            q = new QuScheduleInterface(
                    getAssets().open("Class Schedule Listing.html"),
                    Environment.getExternalStorageDirectory().getPath() + "/qu_data"
            ) {
                @Override
                String in() {
                    return et.getText().toString().trim().toLowerCase();
                }

                @Override
                void url() {
                    try {
                        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(getElem().url)));
                    } catch (Exception e) {
                        println("error!\n");
                    }
                }

                @Override
                void clearText() {
                    tv.setText("");
                }

                @Override
                void println() {
                    if (tv == null || sv == null) return;
                    runOnUiThread(() -> {
                        tv.append("\n");
                        sv.post(() -> sv.fullScroll(View.FOCUS_DOWN));
                    });
                }

                @Override
                void println(Object s) {
                    if (tv == null || sv == null) return;
                    runOnUiThread(() -> {
                        tv.append(s.toString() + "\n");
                        sv.post(() -> sv.fullScroll(View.FOCUS_DOWN));
                    });
                }

                @Override
                void printf(String s, Object... o) {
                    if (tv == null || sv == null) return;
                    runOnUiThread(() -> {
                        tv.append(String.format(s, o));
                        sv.post(() -> sv.fullScroll(View.FOCUS_DOWN));
                    });
                }

                @Override
                void simpleAsync(Runnable async, Runnable post) {
                    new SimpleTask(async, post).execute();
                }
            };
            q.work();
            cal = q.getCal();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void update(View v) {
        q.update();
    }

    public void clear(View v) {
        q.clr();
    }

    public void search(View view) {
        q.search();
    }

    public void room(View view) {
        q.room();
    }

    public void num(View view) {
        q.num();
    }

    public void url(View view) {
        q.url();
    }

    public void free(View view) {
        q.free();
    }

    public void freeNoLab(View view) {
        q.freeNoLab();
    }

    Calendar cal;
    DatePickerDialog dpd;
    TimePickerDialog tpd;

    DatePickerDialog.OnDateSetListener odsl = (view, year, monthOfYear, dayOfMonth) -> {
        cal.set(Calendar.YEAR, year);
        cal.set(Calendar.MONTH, monthOfYear);
        cal.set(Calendar.DAY_OF_MONTH, dayOfMonth);
        if (tpd != null)
            tpd.show(getFragmentManager(), "tpd");
    };

    TimePickerDialog.OnTimeSetListener otsl = (view, hourOfDay, minute, second) -> {
        cal.set(Calendar.HOUR_OF_DAY, hourOfDay);
        cal.set(Calendar.MINUTE, minute);
        cal.set(Calendar.SECOND, second);
    };

    public void setCal(View view) {
        dpd = DatePickerDialog.newInstance(
                odsl,
                cal.get(Calendar.YEAR),
                cal.get(Calendar.MONTH),
                cal.get(Calendar.DAY_OF_MONTH)
        );
        tpd = TimePickerDialog.newInstance(
                otsl,
                cal.get(Calendar.HOUR_OF_DAY),
                cal.get(Calendar.MINUTE),
                false
        );
        dpd.setThemeDark(true);
        dpd.vibrate(false);
        tpd.setThemeDark(true);
        tpd.vibrate(false);
        dpd.show(getFragmentManager(), "dpd");
    }

    public void setCalNow(View view) {
        q.setCalNow();
    }

    @SuppressLint("MissingPermission")
    public void addAllToCal(View view) throws ParseException {
        SimpleDateFormat yyyyMMdd = new SimpleDateFormat("yyyyMMdd", Locale.US);
        Calendar[] startEnd, dateRange;
        String timezone = TimeZone.getDefault().getID();
        int calId = getBestCal(this);
        for (QuSchedule.Elem e : q.getElems()) {
            startEnd = e.parseTime();
            dateRange = e.parseDateRange();
            addToCal(
                    this,
                    false,
                    startEnd[0].getTimeInMillis(),
                    startEnd[1].getTimeInMillis(),
                    e.title,
                    e.where,
                    timezone,
                    calId,
                    "FREQ=WEEKLY;UNTIL=" + yyyyMMdd.format(dateRange[1].getTime())
                            + ";BYDAY=" + e.getDaysTheOtherWay()
            );
        }
    }

    @SuppressLint("MissingPermission")
    public void addToCal(Context c, boolean intent, long start, long end, String title, String location, String timezone, int calId, String rule) {
        if (intent) {
            Intent i = new Intent(Intent.ACTION_INSERT).setData(CalendarContract.Events.CONTENT_URI);
            i.putExtra(CalendarContract.EXTRA_EVENT_BEGIN_TIME, start);
            i.putExtra(CalendarContract.EXTRA_EVENT_END_TIME, end);
            i.putExtra(CalendarContract.Events.TITLE, title);
            i.putExtra(CalendarContract.Events.EVENT_LOCATION, location);
            i.putExtra(CalendarContract.Events.EVENT_TIMEZONE, timezone);
            i.putExtra(CalendarContract.Events.CALENDAR_ID, calId);
            i.putExtra(CalendarContract.Events.HAS_ALARM, 1);
            i.putExtra(CalendarContract.Events.RRULE, rule);
            c.startActivity(i);
        } else {
            ContentValues v = new ContentValues();
            v.put(CalendarContract.Events.DTSTART, start);
            v.put(CalendarContract.Events.DTEND, end);
            v.put(CalendarContract.Events.TITLE, title);
            v.put(CalendarContract.Events.EVENT_LOCATION, location);
            v.put(CalendarContract.Events.EVENT_TIMEZONE, timezone);
            v.put(CalendarContract.Events.CALENDAR_ID, calId);
            v.put(CalendarContract.Events.HAS_ALARM, 1);
            v.put(CalendarContract.Events.RRULE, rule);
            Uri uri = c.getContentResolver().insert(CalendarContract.Events.CONTENT_URI, v);
            if (uri != null)
                calUris.add(uri);
        }
    }

    public static int getBestCal(Context c) {
        String projection[] = {"_id", "calendar_displayName"};
        Uri calendars;
        calendars = Uri.parse("content://com.android.calendar/calendars");
        ContentResolver contentResolver = c.getContentResolver();
        Cursor managedCursor = contentResolver.query(calendars, projection, null, null, null);
        if (managedCursor.moveToFirst()) {
            String calName;
            String calID;
            int nameCol = managedCursor.getColumnIndex(projection[1]);
            int idCol = managedCursor.getColumnIndex(projection[0]);
            do {
                calName = managedCursor.getString(nameCol);
                calID = managedCursor.getString(idCol);
                Log.i("ccc", "name=" + calName + ", id=" + calID);
                if (calName.endsWith("@gmail.com")) {
                    Log.i("ccc", "Chosen ->> name=" + calName + ", id=" + calID);
                    return Integer.parseInt(calID);
                }
            } while (managedCursor.moveToNext());
            managedCursor.close();
        }
        return 1;
    }

    public void deleteFromCal(View view) {
        for (Uri u : calUris)
            getContentResolver().delete(u, null, null);
    }

    public static class SimpleTask extends AsyncTask<Void, Void, Void> {

        Runnable async;
        Runnable post;

        SimpleTask(Runnable async, Runnable post) {
            this.async = async;
            this.post = post;
        }

        @Override
        protected Void doInBackground(Void[] params) {
            if (async != null) async.run();
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            if (post != null) post.run();
        }
    }

}
