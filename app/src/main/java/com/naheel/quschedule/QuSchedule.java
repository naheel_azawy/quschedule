package com.naheel.quschedule;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Locale;

@SuppressWarnings("WeakerAccess")
public class QuSchedule implements Serializable {

    private static final SimpleDateFormat SDF = new SimpleDateFormat("h:mm a", Locale.US);
    private static final SimpleDateFormat SDF_RANGE = new SimpleDateFormat("MMM dd, yyyy", Locale.US);
    private static final SimpleDateFormat SDF_ORG = new SimpleDateFormat("yyyy MM dd", Locale.US);

    public List<Elem> elems;
    public List<Room> rooms;

    InputStream fileInput;
    String dataFilePath;

    public QuSchedule(InputStream fileInput, String dataFilePath) {
        this.fileInput = fileInput;
        this.dataFilePath = dataFilePath;
        File d = new File(dataFilePath);
        if (d.exists()) {
            try {
                ObjectInputStream i = new ObjectInputStream(new FileInputStream(d));
                //noinspection unchecked
                elems = (List<Elem>) i.readObject();
                i.close();
            } catch (IOException | ClassNotFoundException e) {
                e.printStackTrace();
            }
        } else updateHtmlData();
    }

    public List<Elem> roomSchedule(String room, String days) {
        List<Elem> l = new ArrayList<>();
        for (Elem e : elems) {
            if (e.where.contains(room))
                if (oneDayOf(e, days))
                    l.add(e);
        }
        return l;
    }

    public static boolean oneDayOf(Elem e, String days) {
        for (char c : days.toCharArray())
            if (e.days.contains(String.valueOf(c))) return true;
        return false;
    }

    public void updateHtmlData() {
        try {
            InputStream is = fileInput;
            Document doc = Jsoup.parse(
                    is,
                    "utf-8",
                    ""
            );
            is.close();
            Elements elms = doc.getElementsByClass("ddtitle");
            elems = new ArrayList<>(elms.size());
            Element e;
            Elem m;
            Elements children;
            String ds;
            for (int i = 0; i < elms.size(); i++) {
                e = elms.get(i);
                m = new Elem();
                m.title = e.text();
                m.url = e.getElementsByTag("a").get(0).attr("href");
                ds = e.parent().nextElementSibling().child(0).html();
                m.details = ds.replace("<br>", "\n")
                        .substring(0, ds.indexOf("<a") - 28)
                        .replaceAll("<.*\">", "")
                        .replaceAll("</.*>", "");
                try {
                    children = e.parent().nextElementSibling()
                        .child(0)
                        .getElementsByClass("datadisplaytable")
                        .get(0).child(1).child(1).children();
                    m.type = children.get(0).text();
                    m.time = children.get(1).text();
                    m.days = children.get(2).text();
                    m.where = children.get(3).text();
                    m.dateRange = children.get(4).text();
                    m.scheduleType = children.get(5).text();
                    m.instructors = children.get(6).text();
                    elems.add(m); // if couldn't get those info ignore item
                } catch (Exception ignored) {
                }
            }

            ObjectOutputStream o = new ObjectOutputStream(new FileOutputStream(dataFilePath));
            o.writeObject(elems);
            o.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void roomsBuild() {
        rooms = new ArrayList<>();
        Room r;
        for (Elem e : elems) {
            r = roomsFind(e.where);
            if (r != null) {
                r.elems.add(e);
            } else {
                if (!e.where.equals("TBA")) {
                    Room rr = new Room(e.where);
                    rr.elems = new ArrayList<>();
                    rr.elems.add(e);
                    rooms.add(rr);
                }
            }
        }

        Collections.sort(rooms, (o1, o2) -> o1.name.compareTo(o2.name));
    }

    public Room roomsFind(String name) {
        if (rooms == null) roomsBuild();
        for (Room r : rooms) {
            if (r.name.equals(name)) {
                return r;
            }
        }
        return null;
    }

    public List<Room> roomsFindList(String name) {
        if (rooms == null) roomsBuild();
        List<Room> res = new ArrayList<>();
        String[] namearr = name.toLowerCase().split("[\\W]");
        boolean b;
        String rs;
        for (Room r : rooms) {
            b = true;
            rs = r.name.toLowerCase();
            for (String s : namearr) {
                if (!rs.contains(s)) {
                    b = false;
                    break;
                }
            }
            if (b) {
                res.add(r);
            }

        }
        return res;
    }

    public List<Elem> search(String keyword) {
        boolean or = keyword.startsWith("|");
        if (or) keyword = keyword.substring(1);
        String[] arr = keyword.toLowerCase().split("[\\W]");
        List<Elem> res = new ArrayList<>();
        if (or) {
            for (Elem e : elems) {
                for (String s : arr) {
                    if (e.match(s)) {
                        res.add(e);
                        break;
                    }
                }
            }
        } else {
            boolean b;
            for (Elem e : elems) {
                b = true;
                for (String s : arr) {
                    if (!e.match(s)) {
                        b = false;
                        break;
                    }
                }
                if (b) {
                    res.add(e);
                }
            }
        }
        return res;
    }

    public static class Elem implements Serializable {

        public static final boolean ORG_TO_STRING  = false;
        public static final boolean FULL_TO_STRING = true;

        public static final Calendar[] TIME_TBA = new Calendar[2];

        public String
                title,
                url,
                details,
                type,
                time,
                days,
                where,
                dateRange,
                scheduleType,
                instructors;

        private Calendar[] timeParsed = null;
        private Calendar[] dateRangeParsed = null;

        public Calendar[] parseTime() {
            if (timeParsed != null) return timeParsed;
            if (time.equals("TBA")) return TIME_TBA;
            String[] a = time.split(" - ");
            Calendar[] range = parseDateRange();
            Calendar c1 = GregorianCalendar.getInstance();
            Calendar c2 = GregorianCalendar.getInstance();
            try {
                c1.setTime(SDF.parse(a[0]));
                c2.setTime(SDF.parse(a[1]));
                c1.set(Calendar.YEAR, range[0].get(Calendar.YEAR));
                c1.set(Calendar.MONTH, range[0].get(Calendar.MONTH));
                c1.set(Calendar.DAY_OF_MONTH, range[0].get(Calendar.DAY_OF_MONTH));
                c2.set(Calendar.YEAR, range[0].get(Calendar.YEAR));
                c2.set(Calendar.MONTH, range[0].get(Calendar.MONTH));
                c2.set(Calendar.DAY_OF_MONTH, range[0].get(Calendar.DAY_OF_MONTH));
                timeParsed = new Calendar[]{c1, c2};
                return timeParsed;
            } catch (ParseException e1) {
                return null;
            }
        }

        public Calendar[] parseDateRange() {
            if (dateRangeParsed != null) return dateRangeParsed;
            String[] a = dateRange.split(" - ");
            Calendar c1 = GregorianCalendar.getInstance();
            Calendar c2 = GregorianCalendar.getInstance();
            try {
                c1.setTime(SDF_RANGE.parse(a[0]));
                c2.setTime(SDF_RANGE.parse(a[1]));
                dateRangeParsed = new Calendar[]{c1, c2};
                return dateRangeParsed;
            } catch (ParseException e) {
                return null;
            }
        }

        public boolean match(String keyword) {
            return title.toLowerCase().contains(keyword) ||
                    url.toLowerCase().contains(keyword) ||
                    details.toLowerCase().contains(keyword) ||
                    type.toLowerCase().contains(keyword) ||
                    time.toLowerCase().contains(keyword) ||
                    days.toLowerCase().contains(keyword) ||
                    where.toLowerCase().contains(keyword) ||
                    dateRange.toLowerCase().contains(keyword) ||
                    scheduleType.toLowerCase().contains(keyword) ||
                    instructors.toLowerCase().contains(keyword);
        }

        public String getDaysTheOtherWay() {
            StringBuilder sb = new StringBuilder(days.length() * 3);
            for (char c : days.toCharArray()) {
                switch (c) {
                    case 'U':
                        sb.append("SU");
                        break;
                    case 'M':
                        sb.append("MO");
                        break;
                    case 'T':
                        sb.append("TU");
                        break;
                    case 'W':
                        sb.append("WE");
                        break;
                    case 'R':
                        sb.append("TH");
                        break;
                }
                sb.append(',');
            }
            sb.deleteCharAt(sb.length() - 1);
            return sb.toString();
        }

        @Override
        public String toString() {
            return ORG_TO_STRING ? toOrgMode()
                : FULL_TO_STRING ? String.format("title = %s\n" +
                                                 "url = %s\n" +
                                                 "details = %s\n" +
                                                 "type = %s\n" +
                                                 "time = %s\n" +
                                                 "days = %s\n" +
                                                 "where = %s\n" +
                                                 "dateRange = %s\n" +
                                                 "scheduleType = %s\n" +
                                                 "instructors = %s\n",
                                                 title,
                                                 url,
                                                 details,
                                                 type,
                                                 time,
                                                 days,
                                                 where,
                                                 dateRange,
                                                 scheduleType,
                                                 instructors)
                : title;
        }

        public String toOrgMode() {
            String s = "* " + title + " (" + type + ") " +
                time.replaceAll(" ", "") + '\n';
            Calendar[] cals = parseDateRange();
            String range = SDF_ORG.format(cals[0].getTime()) + ' ' +
                SDF_ORG.format(cals[1].getTime());
            int day = -1;
            for (char d : days.toCharArray()) {
                switch (d) {
                case 'U': day = 0; break;
                case 'M': day = 1; break;
                case 'T': day = 2; break;
                case 'W': day = 3; break;
                case 'R': day = 4; break;
                }
                if (day == -1) break;
                s += "  <%%(org-class " + range + " " + day + ")>\n";
            }
            s += "  LOCATION:    " + where + '\n';
            s += "  INSTRUCTORS: " + instructors + '\n';
            s += "  URL:         " + url + '\n';
            s += '\n';
            for (String line : details.split("\n")) {
                s += "  " + line.trim() + '\n';
            }
            return s;
        }
    }

    public class Room implements Serializable {
        public String name;
        public List<Elem> elems;

        public Room(String n) {
            name = n;
        }

        @Override
        public String toString() {
            return name;
        }

        public List<String> timesStr(char day) {
            List<String> res = new ArrayList<>();
            int i = 0;
            for (Elem e : elems)
                if (e.days.contains(String.valueOf(day))) {
                    res.add(i + "- " + e.time);
                    i++;
                }
            return res;
        }

        public List<Calendar[]> timesCal(char day) {
            List<Calendar[]> res = new ArrayList<>();
            for (Elem e : elems)
                if (e.days.contains(String.valueOf(day)))
                    res.add(e.parseTime());
            return res;
        }

        public List<Elem> timesElm(char day) {
            List<Elem> res = new ArrayList<>();
            for (Elem e : elems)
                if (e.days.contains(String.valueOf(day)))
                    res.add(e);
            return res;
        }

        public List<String> timesStr(Calendar cal) {
            return timesStr(dayCalToChar(cal.get(Calendar.DAY_OF_WEEK)));
        }

        public List<Calendar[]> timesCal(Calendar cal) {
            return timesCal(dayCalToChar(cal.get(Calendar.DAY_OF_WEEK)));
        }

        public List<Elem> timesElm(Calendar cal) {
            return timesElm(dayCalToChar(cal.get(Calendar.DAY_OF_WEEK)));
        }

        public boolean isBusy(Calendar cal) { // TODO always false
            for (Calendar[] c : timesCal(cal))
                if (calIsBetween(cal, c[0], c[1]))
                    return true;
            return false;
        }

    }

    public static int dayCharToCal(char c) {
        switch (c) {
            case 'U':
                return Calendar.SUNDAY;
            case 'M':
                return Calendar.MONDAY;
            case 'T':
                return Calendar.TUESDAY;
            case 'W':
                return Calendar.WEDNESDAY;
            case 'R':
                return Calendar.THURSDAY;
            default:
                return -1;
        }
    }

    public static char dayCalToChar(int c) {
        switch (c) {
            case Calendar.SUNDAY:
                return 'U';
            case Calendar.MONDAY:
                return 'M';
            case Calendar.TUESDAY:
                return 'T';
            case Calendar.WEDNESDAY:
                return 'W';
            case Calendar.THURSDAY:
                return 'R';
            default:
                return 'N';
        }
    }

    private static int calToInt(Calendar c) {
        return c.get(Calendar.HOUR_OF_DAY) * 60 + c.get(Calendar.MINUTE);
    }

    private static boolean calIsBetween(Calendar date, Calendar dateStart, Calendar dateEnd) {
        if (date != null && dateStart != null && dateEnd != null) {
            int c = calToInt(date);
            int b = calToInt(dateStart);
            int a = calToInt(dateEnd);
            return c >= b && c < a;
        }
        return false;
    }

}










