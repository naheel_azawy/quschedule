package com.naheel.quschedule;

import java.io.InputStream;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;

public abstract class QuScheduleInterface {

    private Calendar cal;

    private QuSchedule q;
    private List<QuSchedule.Elem> elems;
    private QuSchedule.Elem elem;

    private InputStream fileInput;
    private String dataFilePath;

    public QuScheduleInterface(InputStream fileInput, String dataFilePath) {
        this.fileInput = fileInput;
        this.dataFilePath = dataFilePath;
    }

    abstract String in();

    abstract void url();

    abstract void clearText();

    abstract void println();

    abstract void println(Object s);

    abstract void printf(String s, Object... o);

    abstract void simpleAsync(Runnable async, Runnable post);

    QuSchedule.Elem getElem() {
        return elem;
    }

    List<QuSchedule.Elem> getElems() {
        return elems;
    }

    Calendar getCal() {
        return cal;
    }

    void simpleAsync(Runnable async) {
        simpleAsync(async, null);
    }

    void work(boolean quiet) {
        cal = GregorianCalendar.getInstance();
        if (!quiet) println("Starting...");
        simpleAsync(
                () -> q = new QuSchedule(fileInput, dataFilePath),
                () -> {
                    if (!quiet)
                        printf("done!\nlen = %d\n\n", q.elems.size());
                }
        );
    }

    void work() {
        work(false);
    }

    public void update() {
        println("Loading...");
        simpleAsync(
                () -> q.updateHtmlData(),
                () -> printf("done!\nlen = %d\n\n", q.elems.size())
        );
    }

    public void clr() {
        clearText();
        elem = null;
        elems = null;
    }

    public void search(boolean quiet) {
        simpleAsync(() -> {
            List<QuSchedule.Elem> l = q.search(in());
            elems = l;
            if (l.size() == 0 && !quiet) {
                println("not found!");
            } else {
                if (!quiet)
                    println("Elems -----------");
                int i = 0;
                for (QuSchedule.Elem rr : l) {
                    if (quiet)
                        println(rr.toOrgMode());
                    else
                        printf("%d- %s\n", i++, rr);
                }
                println();
            }
        });
    }

    public void search() {
        search(false);
    }

    public void room() {
        simpleAsync(() -> {
            List<QuSchedule.Room> l = q.roomsFindList(in());
            if (l.size() == 0) {
                println("not found!");
            } else {
                println("Rooms -----------");
                int i = 0;
                for (QuSchedule.Room rr : l) {
                    printf("%d- %s\n", i++, rr);
                }
                QuSchedule.Room r = l.get(0);
                println("1st Room times -----------");
                elems = r.timesElm(cal);
                for (String s : r.timesStr(cal))
                    println(s);
                println("1st Room busy -----------");
                println(r.isBusy(cal));
                println();
            }
        });
    }

    public void num() {
        try {
            println((elem = elems.get(Integer.parseInt(in()))) + "\n");
        } catch (Exception e) {
            println("error: " + e.toString());
        }
    }

    public void free() {
        String t = in();
        simpleAsync(() -> {
            List<QuSchedule.Room> l = q.roomsFindList(t.equals("") ? " " : t);
            if (l.size() == 0) {
                println("not found!");
            } else {
                println("Free rooms -----------");
                int i = 0;
                for (QuSchedule.Room rr : l)
                    if (!rr.isBusy(cal))
                        printf("%d- %s\n", i++, rr);
            }
        });
    }

    public void freeNoLab() {
        String t = in();
        simpleAsync(() -> {
            List<QuSchedule.Room> l = q.roomsFindList(t.equals("") ? " " : t);
            if (l.size() == 0) {
                println("not found!");
            } else {
                println("Free rooms (not labs) -----------");
                int i = 0;
                for (QuSchedule.Room rr : l)
                    if (!rr.isBusy(cal))
                        for (QuSchedule.Elem e : rr.elems)
                            if (e.type.equals("Class")) {
                                printf("%d- %s\n", i++, rr);
                                break;
                            }
            }
        });
    }

    public void setCal(Calendar newCal) {
        cal.setTime(newCal.getTime());
    }

    public void setCalNow() {
        cal = GregorianCalendar.getInstance();
    }

}
