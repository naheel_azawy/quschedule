package com.naheel.quschedule;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;
import java.util.Scanner;

public class CLI extends QuScheduleInterface {

    private static final String PROMPT = "qu-schedule> ";
    private static final String HELP = "Usage: qu [COMMAND] [ARGS]...\n" +
            "List of Main Commands:\n" +
            "s <keyword>: search for a keyword\n" +
            "r <room>: details of a room depending on the time (now by default)\n" +
            "# <number>: view more details on a numbered item from the previous command\n" +
            "free <room>: find free rooms\n" +
            "free-non-lab <room>: find free rooms that are not labs\n" +
            "cal-set <yyyy-MM-dd hh:mm>: set the time\n" +
            "cal-now: set the time to now\n" +
            "update: update data from HTML\n" +
            "help|?|--help|-h|-?: show this help\n" +
            "exit|quit|q: you know..\n" +
            "unknown command: equivalent to 's'\n";
    private static final SimpleDateFormat SDF = new SimpleDateFormat("yyyy-MM-dd hh:mm", Locale.US);

    private String inp = "";

    public static void main(String[] args) {
        try {
            CLI q = new CLI();
            Scanner s = new Scanner(System.in);
            String[] sp = args;
            if (sp.length > 0 && sp[0].equals("org")) {
                q.work(true);
            } else {
                q.work();
            }
            boolean fromArgs = args.length != 0;
            if (!fromArgs) System.out.print(PROMPT);
            boolean run = true;
            char start;
            boolean quote;
            boolean done;
            L:
            while (run) {
                if (!fromArgs)
                    sp = s.nextLine().split(" ");
                if (sp.length >= 2) {
                    start = sp[1].charAt(0);
                    quote = false;
                    if (start == '"' || start == '\'') {
                        sp[1] = sp[1].substring(1);
                        quote = true;
                    }
                    done = false;
                    for (int i = 1; i < sp.length; ++i) {
                        if (done) {
                            err();
                            continue L;
                        }
                        if (quote && sp[i].charAt(sp[i].length() - 1) == start) {
                            sp[i] = sp[i].substring(0, sp[i].length() - 1);
                            done = true;
                        }
                        if (i != 1) sp[1] += " " + sp[i];
                    }
                }
                switch (sp[0]) {
                    case "clear":
                        q.clr();
                        break;
                    case "free":
                        q.inp = sp[1];
                        q.free();
                        break;
                    case "free-non-lab":
                        q.inp = sp[1];
                        q.freeNoLab();
                        break;
                    case "cal-set":
                        Calendar cal = Calendar.getInstance();
                        cal.setTime(SDF.parse(sp[1]));
                        q.setCal(cal);
                        break;
                    case "cal-now":
                        q.setCalNow();
                        break;
                    case "update":
                        q.update();
                        break;
                    case "#":
                        q.inp = sp[1];
                        q.num();
                        break;
                    case "s":
                        q.inp = sp[1];
                        q.search();
                        break;
                    case "org":
                        q.inp = sp[1];
                        q.search(true);
                        break;
                    case "r":
                        q.inp = sp[1];
                        q.room();
                        break;
                    case "help":
                    case "?":
                    case "--help":
                    case "-h":
                    case "-?":
                        q.println(HELP);
                        break;
                    case "exit":
                    case "quit":
                    case "q":
                        run = false;
                        break;
                    case "":
                        break;
                    default:
                        if (sp.length > 1)
                            sp[0] += " " + sp[1];
                        q.inp = sp[0];
                        q.search();
                }
                if (fromArgs) run = false;
                if (run) System.out.print(PROMPT);
            }
            System.out.println();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static void err() {
        System.err.println("Error!");
    }

    private CLI() throws FileNotFoundException {
        super(
                new FileInputStream("Class Schedule Listing.html"),
                "qu_data"
        );
    }

    @Override
    String in() {
        return inp;
    }

    @Override
    void url() {
    }

    @Override
    void clearText() {
        for (int i = 0; i < 50; ++i)
            println();
    }

    @Override
    void println() {
        System.out.println();
    }

    @Override
    void println(Object s) {
        System.out.println(s);
    }

    @Override
    void printf(String s, Object... o) {
        System.out.printf(s, o);
    }

    @Override
    void simpleAsync(Runnable async, Runnable post) {
        if (async != null) async.run();
        if (post != null) post.run();
    }
}
