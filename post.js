let f = document.getElementsByTagName("form")[0];
f.onsubmit = () => {
    let l = [];
    for (let e of f.elements) {
        if (e.name) {
            let v = e.value;
            if (e.options && e.name === "sel_subj") {
                let vs = [];
                for (let o of e.options)
                    vs.push(o.value);
                v = vs.join(',');
            }
            l.push(encodeURIComponent(e.name)+'='+encodeURIComponent(v));
        }
    }
    console.log(l.join('&'));
    return false;
}

