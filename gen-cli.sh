#!/bin/sh
rm -rf cli
OUT="qu.jar"
PKG="com.naheel.quschedule"
PKG_PATH="com/naheel/quschedule/"
JSOUP="jsoup-1.10.2.jar"
DIR=$(pwd)
EXEC=$HOME/GoodStuff/qu.sh
mkdir -p cli
cd cli
mkdir bin
mkdir -p $PKG_PATH
echo "Main-Class: $PKG.CLI
Class-path: $JSOUP" > manifest
cp "../app/src/main/assets/Class Schedule Listing.html" .
cp "../app/libs/$JSOUP" .
cp -r "../app/src/main/java/com" .
rm "$PKG_PATH/MainActivity.java"
javac -cp $JSOUP -d ./bin/ ./$PKG_PATH/*.java
cd bin
jar cvfm ../$OUT ../manifest ./$PKG_PATH/*.class

echo "#!/bin/sh
cd $DIR/cli
java -jar qu.jar \$@" > $EXEC
chmod +x $EXEC

echo "Run as root:"
echo "echo -e '#!/bin/sh\\n$EXEC \$@' > /bin/qu && chmod +x /bin/qu"


